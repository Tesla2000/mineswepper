import cv2
import numpy as np

from tile import Tile


def translate_to_tiles(squares):
    rows = 20
    columns = 24
    board = np.empty((rows, columns), dtype=Tile)
    for row in range(rows):
        for column in range(columns):
            m = np.mean(squares[row, column, 5:20, 5:20])
            if m == 179 or m == 186:
                board[row][column] = Tile(False, row, column, None, None)
            elif m == 142.79111111111112 or m == 146.45333333333335:
                board[row][column] = Tile(True, row, column, None, None)
            elif m == 200 or m == 190:
                board[row][column] = Tile(False, row, column, 0, None)
            elif m == 171.15555555555557 or m == 179.08:
                board[row][column] = Tile(False, row, column, 1, None)
            elif m == 169.48444444444445 or m == 177.14222222222222:
                board[row][column] = Tile(False, row, column, 2, None)
            elif m == 166.51111111111112 or m == 174.15555555555557:
                board[row][column] = Tile(False, row, column, 3, None)
            elif m == 157.54222222222222 or m == 164.88444444444445:
                board[row][column] = Tile(False, row, column, 4, None)
            elif m == 182.78222222222223 or m == 190.37777777777777:
                board[row][column] = Tile(False, row, column, 5, None)
            elif m == 171.62666666666667:
                board[row][column] = Tile(False, row, column, 6, None)
            else:
                print(m)
                board[row][column] = Tile(False, row, column, 6, None)
    for row in range(rows):
        for column in range(columns):
            board[row][column].neighbours = tuple(filter(lambda tile: tile != False, ((board[i][j] if i != row or j != column else False) for i in range(max(row - 1, 0), min(row + 2, rows)) for j in range(max(column - 1, 0), min(column + 2, columns)))))
    return board


def divide_picture_to_squares(picture):
    return np.array(tuple(
        tuple(picture[25 * row:25 * (row + 1), 25 * column:25 * (column + 1)] for column in range(24)) for row in
        range(20)))


# tuple(tuple(np.mean(np.array(tuple(         tuple(picture[25 * row:25 * (row + 1), 25 * column:25 * (column + 1)] for column in range(24)) for row in         range(20)))[row, column, 5:20, 5:20]) for column in range(24)) for row in range(20))
def load_picture():
    return cv2.cvtColor(cv2.imread("canvas.png"), cv2.COLOR_BGR2GRAY)
