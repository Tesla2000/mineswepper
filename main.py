import time

import undetected_chromedriver as uc
from selenium.common import ElementClickInterceptedException

from selenium_functions import get_canvas_image, wait_and_click, click_on_canvas
from utility_functions import load_picture, divide_picture_to_squares, translate_to_tiles


def tile_active_new(tile):
    return not tile.mined and tile.remaining_mines_around is None and any \
        (map(lambda tile: tile.remaining_mines_around, tile.neighbours))


def tile_active(tile):
    return not tile.mined and tile.remaining_mines_around and any(
        map(lambda tile: not tile.mined and tile.remaining_mines_around is None, tile.neighbours))


def get_state(web_driver=None):
    if web_driver is None:
        options = uc.ChromeOptions()
        options.add_argument(f'--start-maximized')
        web_driver = uc.Chrome(options=options)
        web_driver.get(
            'https://www.google.com/search?q=saper&oq=saper&aqs=chrome.0.35i39i355j46i39i131i433i512j0i131i433i512j0i433i512l2j0i131i433i512j0i512l3.2258j0j7&sourceid=chrome&ie=UTF-8')
        wait_and_click(web_driver, '//*[text()="Odrzuć wszystko"]')
        try:
            counter = 9
            while wait_and_click(web_driver, '//*[@src="//www.google.com/logos/fnbx/minesweeper/cta.png"]'):
                if not counter:
                    web_driver.refresh()
                    counter = 9
                counter -= 1
        except ElementClickInterceptedException:
            pass
        wait_and_click(web_driver, '//*[text()="Średni"]')
        wait_and_click(web_driver, '//*[text()="Trudny"]')
    get_canvas_image(web_driver)
    return web_driver


def get_fields_next_to_active(board):
    return list(filter(tile_active_new, board.flatten()))


def get_fields_active(board):
    return tuple(filter(tile_active, board.flatten()))


def possible(next_to, active):
    index = 0
    while True:
        tile = next_to[index]
        if tile.remaining_mines_around:
            tile.remaining_mines_around = None
            index -= 1
            if index == -1:
                return False
            continue
        elif tile.mined:
            tile.mined = False
            tile.remaining_mines_around = 1
        elif not tile.mined:
            tile.mined = True
        for neighbour in filter(lambda neighbour: neighbour in active, tile.neighbours):
            if len(tuple(filter(lambda neighbour: neighbour.mined, neighbour.neighbours))) > neighbour.remaining_mines_around \
                    or len(tuple(filter(lambda neighbour: neighbour.mined or neighbour.remaining_mines_around is None, neighbour.neighbours))) < neighbour.remaining_mines_around:
                break
        else:
            index += 1
        if index == len(next_to):
            for tile in next_to:
                tile.mined = False
                tile.remaining_mines_around = None
            return True


def check(web_driver, row, column, mine=False):
    click_on_canvas(web_driver, column*25+12-300, row*25+12-250, mine)


def possible_empty(tile):
    for neighbour in filter(lambda neighbour: neighbour.remaining_mines_around, tile.neighbours):
        if len(tuple(filter(lambda neighbour: neighbour.mined or neighbour.remaining_mines_around is None, neighbour.neighbours))) - 1 < neighbour.remaining_mines_around:
            return False
    return True


def possible_mine(tile):
    for neighbour in filter(lambda neighbour: neighbour.remaining_mines_around, tile.neighbours):
        if len(tuple(filter(lambda neighbour: neighbour.mined, neighbour.neighbours))) >= neighbour.remaining_mines_around:
            return False
    return True


if __name__ == '__main__':
    web_driver = get_state(None)
    while True:
        check(web_driver, 0, 0)
        time.sleep(1)
        get_state(web_driver)
        board = translate_to_tiles(divide_picture_to_squares(load_picture()))
        next_to = get_fields_next_to_active(board)
        active = get_fields_active(board)
        for tile in next_to:
            if not possible_empty(tile):
                check(web_driver, tile.row, tile.column, True)
            if not possible_mine(tile):
                check(web_driver, tile.row, tile.column)
        for _ in range(len(next_to)):
            tile = next_to.pop(0)
            tile.mined = True
            if not possible(next_to, active):
                check(web_driver, tile.row, tile.column)
                break
            tile.mined = False
            next_to.append(tile)
