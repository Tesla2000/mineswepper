from dataclasses import dataclass


@dataclass()
class Tile:
    mined: bool
    row: int
    column: int
    remaining_mines_around: int
    neighbours: tuple

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return str(self.mined if self.mined else self.remaining_mines_around) + f',{self.row},{self.column}'