import base64
from time import sleep

from numpy.random import normal
from selenium.common.exceptions import StaleElementReferenceException, \
    NoSuchElementException, TimeoutException
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


def sleep_normal(multiplier=2):
    sleep(multiplier * normal_random_time())


def normal_random_time():
    time = normal(0.5, 0.5, (1,))[0]
    return time if time > 0.2 else normal_random_time()


def wait_and_click(web_driver, value, by=By.XPATH, seconds=1):
    try:
        WebDriverWait(web_driver, seconds).until(
            EC.element_to_be_clickable(
                wait(web_driver, value, seconds, by))).click()
        sleep_normal()
    except (NoSuchElementException, TimeoutException, StaleElementReferenceException) as _:
        return False
    return True


def wait_and_click_js(web_driver, value, time=1, by=By.XPATH):
    element = wait(web_driver, value, time, by)
    web_driver.execute_script("arguments[0].click();", element)


def get_text_of_element(web_driver, value, by=By.XPATH, seconds=1):
    return wait(web_driver, value, seconds, by).text


def wait(web_driver, value, seconds=1, by=By.XPATH):
    web_driver.implicitly_wait(seconds)
    element = web_driver.find_element(
        by, value)
    return element


def element_present(web_driver, value, seconds=1, by=By.XPATH):
    try:
        wait(web_driver, value, seconds, by)
        return True
    except NoSuchElementException:
        return False


def get_attribute(web_driver, value, dtype, by=By.XPATH, seconds=10):
    return wait(web_driver, value, seconds, by).get_attribute(dtype)


def all_elements(web_driver, element, by=By.XPATH):
    return web_driver.find_elements(by=by, value=element)


def wait_and_send(web_driver, value, keys, by=By.XPATH, time=10):
    try:
        element = wait(web_driver, value, time, by)
        element.clear()
        element.send_keys(str(keys))
        sleep_normal()
    except:
        return False
    return True


def time_to_seconds(time_string):
    time_string = time_string.split(":")[::-1]
    return sum(int(time_string[i]) * (60 ** i) for i in range(3))


def get_canvas_image(web_driver):
    canvas = wait(web_driver, '//canvas')
    canvas_base64 = web_driver.execute_script("return arguments[0].toDataURL('image/png').substring(21);", canvas)
    with open("canvas.png", 'wb') as file:
        file.write(base64.b64decode(canvas_base64))


def click_on_canvas(web_driver, offset_x, offset_y, right=False):
    canvas = wait(web_driver, '//canvas')
    if right:
        ActionChains(web_driver).move_to_element(canvas).move_by_offset(offset_x, offset_y).context_click().release().perform()
    else:
        ActionChains(web_driver).move_to_element(canvas).move_by_offset(offset_x, offset_y).click().release().perform()

